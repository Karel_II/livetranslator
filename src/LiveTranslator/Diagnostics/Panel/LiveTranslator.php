<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator\Diagnostics\Panel;

use \LiveTranslator\Translator,
    \LiveTranslator\Latte\Runtime\FilterOrdinal,
    \LiveTranslator\LiveTranslatorString;
use \Tracy\IBarPanel;
use \Nette\Http\IRequest;

/**
 * Description of LiveTranslator
 *
 * @author Karel
 * 
 * @property-write string $layout Description
 */
class LiveTranslator implements IBarPanel {

    use \Nette\SmartObject;

    /**
     * DATA
     */
    const
            DATA_LANGUAGE_KEY = 'X-LiveTranslator-Lang',
            DATA_NAMESPACE_KEY = 'X-LiveTranslator-Ns',
            DATA_STRINGS_KEY = 'X-LiveTranslator-Strings',
            DATA_LANGUAGES_URL_KEY = 'X-LiveTranslator-LangUrl';

    /**
     * HEADER
     */
    const
            HEADER_XHR_KEY = 'X-LiveTranslator-Client',
            HEADER_TRACY_KEY = 'X-Tracy-Ajax';

    /**
     * LAYOUT
     */
    const
            LAYOUT_VERTICAL = 'vertical',
            LAYOUT_HORIZONTAL = 'horizontal';

    /** @var string */
    protected $layout = self::LAYOUT_VERTICAL;

    /** @var Translator */
    protected $translator;

    /** @var \Nette\Http\IRequest */
    protected $httpRequest;

    /**
     * 
     * @param \LiveTranslator\Translator $translator
     * @param \Nette\Http\IRequest $httpRequest
     */
    public function __construct(Translator $translator, IRequest $httpRequest) {
        $this->translator = $translator;
        $this->httpRequest = $httpRequest;
        $this->processRequest();
    }

    /**
     * 
     * @param string $name
     * @return string
     */
    protected function getLatteName($name) {
        $m = array();
        preg_match('#(\w+)$#', get_class($this), $m);
        return lcfirst($m[1]) . ucfirst($name) . '.phtml';
    }

    /**
     * 
     * @return \Latte\Engine
     */
    protected function initEngine() {
        $latte = new \Latte\Engine();
        $latte->addFilter('ordinal', new FilterOrdinal);
        return $latte;
    }

    /**
     * Return panel ID
     * @return string
     */
    protected function getId() {
        return substr(md5(spl_object_hash($this)), 0, 4);
    }

    /**
     * 
     * @param string $layout
     * @return $this
     * @throws \Nette\InvalidArgumentException
     */
    public function setLayout($layout = self::LAYOUT_VERTICAL) {
        if ($layout === self::LAYOUT_VERTICAL || $layout === self::LAYOUT_HORIZONTAL) {
            $this->layout = $layout;
            return $this;
        }
        throw new \Nette\InvalidArgumentException("Wrong layout, $layout is not " . self::LAYOUT_VERTICAL . " or " . self::LAYOUT_HORIZONTAL . " !");
    }

    /**
     * Returns the code for the panel tab.
     * @return string
     */
    public function getTab() {
        $latte = $this->initEngine();
        $parameters = array();
        $parameters['id'] = $this->getId();
        $parameters['untranslatedCount'] = $this->translator->countUntranslatedStrings();
        $template = $latte->renderToString(__DIR__ . '/' . $this->getLatteName('Tab'), $parameters);
        return $template;
    }

    /**
     * Returns the code for the panel.
     * @return string
     */
    public function getPanel() {
        $latte = $this->initEngine();
        $currentLanguage = $this->translator->getCurrentLanguage();

        $parameters = array();
        $parameters['id'] = $this->getId();
        $parameters['language'] = $currentLanguage;
        $parameters['isDefaultLang'] = $this->translator->isCurrentLangDefault();

        $parameters['stringNounNumbersCount'] = $this->translator->getVariantsCount();
        $parameters['liveTranslatorStrings'] = $this->translator->getAllStringsToArray();

        /** layout */
        $parameters['layout'] = $this->layout;

        $parameters['tracyControlHandler'] = 'liveTranslatorTracy-getLanguages';

        /** json setup */
        $parameters['jsonData'] = array(
            self::DATA_LANGUAGE_KEY => $currentLanguage->name,
            self::DATA_NAMESPACE_KEY => $this->translator->namespace,
            self::DATA_STRINGS_KEY => array()
        );
        $parameters['jsonDataStringsKey'] = self::DATA_STRINGS_KEY;
        $parameters['jsonDataSLanguageUrlKey'] = self::DATA_LANGUAGES_URL_KEY;
        $parameters['jsonHeaders'] = array(
            self::HEADER_XHR_KEY => 'devel',
            self::HEADER_TRACY_KEY => '',
        );


        $template = $latte->renderToString(__DIR__ . '/' . $this->getLatteName('Panel'), $parameters);
        return $template;
    }

    /**
     * Handles incoming request and sets translations.
     */
    private function processRequest() {
        if ($this->httpRequest->isMethod(IRequest::POST) && $this->httpRequest->isAjax() && $this->httpRequest->getHeader(self::HEADER_XHR_KEY) && ($data = $this->httpRequest->getPost(self::DATA_STRINGS_KEY)) !== NULL) {
            $language = $this->httpRequest->getPost(self::DATA_LANGUAGE_KEY);
            $namespace = $this->httpRequest->getPost(self::DATA_NAMESPACE_KEY);

            $this->translator->setCurrentLanguage($language);
            if ($namespace) {
                $this->translator->setNamespace($namespace);
            }
            $liveTranslatorStrings = array();
            foreach ($data as $translated) {
                $liveTranslatorString = LiveTranslatorString::fromRequest($translated);
                $liveTranslatorStrings[$liveTranslatorString->id] = $liveTranslatorString;
                $this->translator->storeString($liveTranslatorString);
            }
        }
    }

}
