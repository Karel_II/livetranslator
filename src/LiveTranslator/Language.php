<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator;

/**
 * Description of LiveTranslatorLanguage
 *
 * @author Karel
 * @property-read string $name Description
 */
class Language {

    use \Nette\SmartObject;

    const
            REGULAR_LANGUAGE = '[a-z]{2}([-_][A-Z]{2,6})?',
            REGULAR_FLOAT = '[+-]?([0-9]*[.])?[0-9]+',
            REGULAR_LESS = 'a?l(t|e)',
            REGULAR_GREATER = 'a?g(t|e)';

    private $name = NULL;

    /** @var string plural-form meta */
    public static $defaultPluralForms = '1;;';

    /**
     * 0-singular only, 1-singular+plural, > 1 more than one plural;
     * @var int 
     */
    private $nounNumbersCount = 0;

    /**
     *
     * @var type 
     */
    private $nounNumbers = array();

    /**
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    public function getNounNumbersCount() {
        return $this->nounNumbersCount;
    }

    public static function fromConfig($configKey, $configString) {
        $return = new static;
        $matchesBase = array();
        if (preg_match('/'.self::REGULAR_LANGUAGE.'/', $configKey) && preg_match('/^(?<nounNumbersCount>[0-9]+);(?<nounNumber>([0-9.+-aglte]+\|)*)(?<nounNumberLast>[0-9.+-aglte]*);/i', $configString, $matchesBase)) {
            $matchesExtends = array();
            if (!empty($matchesBase['nounNumber']) && preg_match_all('/(?<nounNumber>[0-9.+-aglte]+)\|/iu', $matchesBase['nounNumber'], $matchesExtends)) {
                foreach ($matchesExtends['nounNumber'] as $nounNumber) {
                    $return->nounNumbers[] = $nounNumber;
                }
            }
            $return->nounNumbers[] = $matchesBase['nounNumberLast'] ?: TRUE;
            $return->nounNumbersCount = $matchesBase['nounNumbersCount'];
            $return->name = $configKey;
        } elseif (is_integer($configKey) && preg_match('/[a-z-_]{2,10}/i', $configString)) {
            $return->nounNumbersCount = 1;
            $return->nounNumbers[] = TRUE;
            $return->name = $configString;
        } else {
            throw new \Exception;
        }
        return $return;
    }

    protected static function mathTest($value, $valueRef, $relation) {
        switch ($relation) {
            case 'alt':
                return ($valueRef > abs($value));
            case 'ale':
                return($valueRef >= abs($value));
            case 'age':
                return($valueRef <= abs($value));
            case 'agt':
                return($valueRef < abs($value));
            case 'lt':
                return ($valueRef > $value);
            case 'le':
                return($valueRef >= $value);
            case 'ge':
                return($valueRef <= $value);
            case 'gt':
                return($valueRef < $value);
        }
        throw new \Exception;
    }

    public function getNounNumber($count) {
        if (!is_numeric($count)) {
            return NULL;
        }
        foreach ($this->nounNumbers as $key => $nounNumber) {
            $match = array();
            if (preg_match('/^(?<number1>' . self::REGULAR_FLOAT . ')$/', $nounNumber, $match) && ($match['number1'] === $count)) {
                return $key;
            } else if (preg_match('/^\.(?<operand2>' . self::REGULAR_LESS . ')(?<number2>' . self::REGULAR_FLOAT . ')$/', $nounNumber, $match) && self::mathTest($count, $match['number2'], $match['operand2'])) {
                return $key;
            } else if (preg_match('/^(?<number1>' . self::REGULAR_FLOAT . ')(?<operand1>' . self::REGULAR_GREATER . ')\.$/', $nounNumber, $match) && self::mathTest($count, $match['number1'], $match['operand1'])) {
                return $key;
            } else if (preg_match('/^(?<number1>' . self::REGULAR_FLOAT . ')(?<operand1>' . self::REGULAR_GREATER . ')((?<operand2>' . self::REGULAR_LESS . '))(?<number2>' . self::REGULAR_FLOAT . ')$/', $nounNumber, $match) && self::mathTest($count, $match['number1'], $match['operand1']) && self::mathTest($count, $match['number2'], $match['operand2'])) {
                return $key;
            } else if ($nounNumber === TRUE) {
                return $key;
            }
        }
        return NULL;
    }

}
