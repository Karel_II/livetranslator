<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator\Application\UI;

/**
 *
 * @author Karel
 */
interface ITracyControlFactory {

    /**
     * 
     * @return \LiveTranslator\Application\UI\TracyControl
     */
    public function create($languageVariableName);
}
