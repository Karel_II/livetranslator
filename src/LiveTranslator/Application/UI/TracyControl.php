<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator\Application\UI;

use LiveTranslator\Translator,
    LiveTranslator\Diagnostics\Panel;
use Nette\Utils\Html,
    Nette\Application\Responses\JsonResponse;

/**
 * Description of TracyControl
 *
 * @author Karel
 */
class TracyControl extends \Nette\Application\UI\Control {

    /**
     *
     * @var string 
     */
    private $languageVariableName = 'language';

    /**
     *
     * @var \LiveTranslator\Translator 
     */
    private $translator = NULL;

    function __construct($languageVariableName, Translator $translator) {
        $this->languageVariableName = $languageVariableName;
        $this->translator = $translator;
    }

    public function handleGetLanguages() {
        $presenter = $this->getPresenter();
        if ($presenter->isAjax()) {
            $availableLanguages = $this->translator->getAvailableLanguagesNames();
            $return = '';
            foreach ($availableLanguages as $value) {
                $link = $presenter->link('this', [$this->languageVariableName => $value]);
                $return .= Html::el('a')->href($link)->setText($value) . "\n";
            }
            $presenter->sendResponse(new JsonResponse([Panel\LiveTranslator::DATA_LANGUAGES_URL_KEY => $return]));
            exit;
        }
        $presenter->redirect('Homepage');
    }

}
