<?php

namespace LiveTranslator;

use Nette,
    Nette\Utils\Validators;
use LiveTranslator\LiveTranslatorString,
    LiveTranslator\Language;

/**
 * Translator implementation.
 *
 * @author Vladislav Hejda
 *
 * @property string $namespace
 * @property string $currentLang
 * @property array $availableLanguages
 * @property string $presenterLanguageParam
 * @property-read bool $currentLangDefault
 */
class Translator implements Nette\Localization\ITranslator {

    use Nette\SmartObject;

    /* @var string */

    private $namespace;

    /** @var \LiveTranslator\Language */
    private $currentLanguage = NULL;

    /** @var \LiveTranslator\Language */
    private $defaultLanguage = NULL;

    /** @var array */
    private $availableLanguages = array();

    /** @var IStorage */
    private $translatorTranslatedStorage;

    /** @var IStorage */
    private $translatorUntranslatedStorage;

    /**
     * @param ITranslatorStorage $translatorTranslatedStorage
     * @param ITranslatorStorage $translatorUntranslatedStorage
     */
    public function __construct(IStorage $translatorTranslatedStorage, IStorage $translatorUntranslatedStorage) {
        $this->translatorTranslatedStorage = $translatorTranslatedStorage;
        $this->translatorUntranslatedStorage = $translatorUntranslatedStorage;
    }

    /**
     * 
     * @param array $liveTranslatorLanguageConfig
     * @return $this
     */
    public function setupAvailableLanguage(array $liveTranslatorLanguageConfig) {
        foreach ($liveTranslatorLanguageConfig as $configKey => $configString) {
            $liveTranslatorLanguage = Language::fromConfig($configKey, $configString);
            $this->availableLanguages[$liveTranslatorLanguage->name] = $liveTranslatorLanguage;
        }
        return $this;
    }

    /**
     * 
     * @return type
     */
    function getNamespace() {
        return $this->namespace;
    }

    /**
     * 
     * @return \LiveTranslator\Language
     */
    function getCurrentLanguage(): \LiveTranslator\Language {
        if ($this->currentLanguage !== NULL) {
            return $this->currentLanguage;
        }
        return $this->currentLanguage = $this->defaultLanguage;
    }

    /**
     * 
     * @return \LiveTranslator\Language
     */
    function getDefaultLanguage(): \LiveTranslator\Language {
        return $this->defaultLanguage;
    }

    /**
     * 
     * @return array
     */
    public function getAvailableLanguagesNames() {
        $return = array();
        foreach ($this->availableLanguages as $language) {
            $return[] = $language->name;
        }
        return $return;
    }

    /**
     * @return bool
     */
    public function isCurrentLangDefault() {
        return $this->getCurrentLanguage() === $this->getDefaultLanguage();
    }

    /**
     * 
     * @param string $namespace
     * @return $this
     */
    function setNamespace($namespace) {
        Validators::assert($namespace, 'string:1..', '$namespace');
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * 
     * @param \LiveTranslator\Language|string $currentLanguage
     * @return \LiveTranslator\Translator
     */
    function setCurrentLanguage($currentLanguage) {
        if (($currentLanguage instanceof \LiveTranslator\Translator) && isset($this->availableLanguages[$currentLanguage->name])) {
            $this->currentLanguage = $this->availableLanguages[$currentLanguage->name];
        } elseif (Validators::is($currentLanguage, 'string:1..') && isset($this->availableLanguages[$currentLanguage])) {
            $this->currentLanguage = $this->availableLanguages[$currentLanguage];
        } else {
            throw new TranslatorException("Language $currentLanguage is empty or not available.");
        }
        return $this;
    }

    /**
     * 
     * @param \LiveTranslator\Language|string $defaultLanguage
     * @return \LiveTranslator\Translator
     */
    function setDefaultLanguage($defaultLanguage) {
        if (($defaultLanguage instanceof \LiveTranslator\Translator) && isset($this->availableLanguages[$defaultLanguage->name])) {
            $this->defaultLanguage = $this->availableLanguages[$defaultLanguage->name];
        } elseif (Validators::is($defaultLanguage, 'string:1..') && isset($this->availableLanguages[$defaultLanguage])) {
            $this->defaultLanguage = $this->availableLanguages[$defaultLanguage];
        } else {
            throw new TranslatorException("Language $defaultLanguage is empty or not available.");
        }
        return $this;
    }

    /*     * **********************************************************************

      /**
     * @param string $lang
     * @return array (nplurals, plural)
     */

    public function getVariantsCount($languageName = NULL) {
        $language = (!empty($languageName) && $this->availableLanguages[$languageName] !== NULL) ? $this->availableLanguages[$languageName] : $this->getCurrentLanguage();
        return $language->getNounNumbersCount();
    }

    /**
     * @param int $count
     * @param string $lang
     * @return int
     */
    public function getVariant($count, $languageName = NULL) {
        $language = (!empty($languageName) && $this->availableLanguages[$languageName] !== NULL) ? $this->availableLanguages[$languageName] : $this->getCurrentLanguage();
        return $language->getNounNumber($count);
    }

    /**
     * Translates string.
     * Give original string or array of its original variants.
     * Rest of arguments are handed to sprintf() function.
     * @param string|array $string
     * @param int $count
     * @return string
     * @throws TranslatorException
     */
    public function translate($string, $count = NULL) {
        $hasVariants = FALSE;
        if (is_array($string)) {
            $hasVariants = TRUE;
            $stringVariants = array_map('trim', array_values($string));
            $string = trim((string) $string[0]);
        } else {
            $string = trim((string) $string);
            $plural = 0;
            if (is_array($count)) {
                $args = $count;
            } elseif (func_num_args() > 2) {
                $args = func_get_args();
                unset($args[0]);
                $args = array_values($args);
            } elseif ($count === NULL) {
                $args = NULL;
            } else {
                $args = array($count);
            }
        }

        if ($hasVariants) {
            if (is_array($count)) {
                $args = $count;
            } elseif (($argc = func_num_args()) > 2) {
                $args = func_get_args();
                unset($args[0]);
                $args = array_values($args);
            } if (isset($args)) {
                unset($count);
                foreach ($args as $arg) {
                    if (is_numeric($arg)) {
                        $count = (int) $arg;
                        break;
                    }
                }
                if (!isset($count)) {
                    $count = 1;
                }
            } else {
                if (is_numeric($count)) {
                    $count = (int) $count;
                } else {
                    $count = 1;
                }
                $args = array($count);
            }

            $plural = $this->getVariant($count);
        }

        $language = $this->getCurrentLanguage();

        $lang = $language->name;

        if ($lang === $this->defaultLanguage->name) {
            if ($hasVariants) {
                if (isset($stringVariants[$plural])) {
                    $translated = $stringVariants[$plural];
                } else {
                    $translated = end($stringVariants);
                }
            } else {
                $translated = $string;
            }
        } else {
            $language = $this->getCurrentLanguage();
            $liveTranslatorString = LiveTranslatorString::newString($string, false, $language->name, false);
            if ($this->translatorTranslatedStorage->fetchLiveTranslatorString($liveTranslatorString, $this->namespace) && $liveTranslatorString->translated && ($translated = $liveTranslatorString->getTranslation($plural))) {
                
            } else {
                $this->translatorUntranslatedStorage->setLiveTranslatorString($liveTranslatorString, $this->namespace);
                if ($hasVariants) {
                    if (isset($stringVariants[$plural])) {
                        $translated = $stringVariants[$plural];
                    } else {
                        $translated = end($stringVariants);
                    }
                } else {
                    $translated = $string;
                }
            }
        }

        if ($args !== NULL AND FALSE !== strpos($translated, '%')) {
            $tmp = str_replace(array('%label', '%name', '%value'), array('#label', '#name', '#value'), $translated);
            if (FALSE !== strpos($tmp, '%')) {
                $translated = vsprintf($tmp, $args);
                $translated = str_replace(array('#label', '#name', '#value'), array('%label', '%name', '%value'), $translated);
            }
        }

        return $translated;
    }

    /*     * *******************************************************************
     * 
     *                              STORAGE
     * 
     * *********************************************************************** */

    public function countUntranslatedStrings() {
        $language = $this->getCurrentLanguage();
        return $this->translatorUntranslatedStorage->countLiveTranslatorStrings($language, $this->namespace);
    }

    /**
     * 
     * @return array
     */
    public function getAllStrings() {
        $return = array();
        $language = $this->getCurrentLanguage();
        $translatedStrings = $this->translatorTranslatedStorage->getAllLiveTranslatorStrings($language, $this->namespace);
        foreach ($translatedStrings as $translatedString) {
            $return[$translatedString->id] = $translatedString;
        }
        $untranslatedStrings = $this->translatorUntranslatedStorage->getAllLiveTranslatorStrings($language, $this->namespace);
        foreach ($untranslatedStrings as $untranslatedString) {
            $return[$untranslatedString->id] = $untranslatedString;
        }
        return $return;
    }

    /**
     * 
     * @return array
     */
    public function getAllStringsToArray() {
        $return = array();
        $language = $this->getCurrentLanguage();
        $translatedStrings = $this->translatorTranslatedStorage->getAllLiveTranslatorStrings($language, $this->namespace);
        foreach ($translatedStrings as $translatedString) {
            $return[$translatedString->id] = $translatedString->toArray();
        }
        $untranslatedStrings = $this->translatorUntranslatedStorage->getAllLiveTranslatorStrings($language, $this->namespace);
        foreach ($untranslatedStrings as $untranslatedString) {
            $return[$untranslatedString->id] = $untranslatedString->toArray();
        }
        return $return;
    }

    /**
     * Store string.
     * @param \LiveTranslator\LiveTranslatorString $liveTranslatorString
     * @return $this
     */
    public function storeString(LiveTranslatorString $liveTranslatorString) {
        if ($liveTranslatorString->isTranslated()) {
            $this->translatorTranslatedStorage->setLiveTranslatorString($liveTranslatorString, $this->namespace);
            $this->translatorUntranslatedStorage->removeLiveTranslatorString($liveTranslatorString, $this->namespace);
        } else {
            $this->translatorTranslatedStorage->removeLiveTranslatorString($liveTranslatorString, $this->namespace);
            $this->translatorUntranslatedStorage->setLiveTranslatorString($liveTranslatorString, $this->namespace);
        }
        return $this;
    }

}

class TranslatorException extends \Exception {
    
}
