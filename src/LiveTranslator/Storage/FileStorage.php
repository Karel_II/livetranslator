<?php

namespace LiveTranslator\Storage;

use LiveTranslator\IStorage,
    LiveTranslator\LiveTranslatorString,
    LiveTranslator\Language;

class FileStorage implements IStorage {

    /**
     *
     * @var string 
     */
    private $namespace = 'default';

    /** @var string */
    protected $storageDir;

    /** @var resource[] */
    protected $handlers = array();

    /**
     *
     * @var array 
     */
    private $internalStorage = array();

    /**
     *
     * @var array 
     */
    private $internalStorageUpdated = array();

    /**
     * @param string $storageDir
     * @throws \Nette\DirectoryNotFoundException
     */
    public function __construct($storageDir) {
        $this->storageDir = realpath($storageDir);

        if (FALSE === $this->storageDir) {
            throw new \Nette\DirectoryNotFoundException("Directory $storageDir was not found.");
        }
    }

    /**
     * 
     * @param Language $language
     * @param string $namespace
     * @return array
     */
    public function getAllLiveTranslatorStrings(Language $language, $namespace = NULL): array {
        if (!isset($this->internalStorage[($namespace ?: $this->namespace)][$language->name])) {
            $this->readDatafile($language->name, ($namespace ?: $this->namespace));
        }
        if (isset($this->internalStorage[($namespace ?: $this->namespace)][$language->name])) {
            return $this->internalStorage[($namespace ?: $this->namespace)][$language->name];
        }
        return array();
    }

    /**
     * 
     * @param string $id
     * @param Language $language
     * @param string $namespace
     * @return LiveTranslatorString
     */
    public function getLiveTranslatorString($id, Language $language, $namespace = NULL): LiveTranslatorString {
        if (!isset($this->internalStorage[($namespace ?: $this->namespace)][$language->name])) {
            $this->readDatafile($language->name, ($namespace ?: $this->namespace));
        }
        if (isset($this->internalStorage[($namespace ?: $this->namespace)][$language->name][$id])) {
            return $this->internalStorage[($namespace ?: $this->namespace)][$language->name][$id];
        }
        return NULL;
    }

    /**
     * 
     * @param Language $language
     * @param string $namespace
     * @return int Description
     */
    public function countLiveTranslatorStrings(Language $language, $namespace = NULL): int {
        if (!isset($this->internalStorage[($namespace ?: $this->namespace)][$language->name])) {
            $this->readDatafile($language->name, ($namespace ?: $this->namespace));
        }
        if (isset($this->internalStorage[($namespace ?: $this->namespace)][$language->name])) {
            return count($this->internalStorage[($namespace ?: $this->namespace)][$language->name]);
        }
        return 0;
    }

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return bool
     */
    public function fetchLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): bool {
        if (!isset($this->internalStorage[($namespace ?: $this->namespace)][$liveTranslatorString->language])) {
            $this->readDatafile($liveTranslatorString->language, ($namespace ?: $this->namespace));
        }
        if (isset($this->internalStorage[($namespace ?: $this->namespace)][$liveTranslatorString->language][$liveTranslatorString->id])) {
            $liveTranslatorStringFetch = $this->internalStorage[($namespace ?: $this->namespace)][$liveTranslatorString->language][$liveTranslatorString->id];
            $liveTranslatorString->translations = $liveTranslatorStringFetch->translations;
            $liveTranslatorString->deleted = $liveTranslatorStringFetch->deleted;
            return true;
        }
        $liveTranslatorString->translations = false;
        return false;
    }

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return IStorage
     */
    public function setLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): IStorage {
        if (!isset($this->internalStorage[($namespace ?: $this->namespace)][$liveTranslatorString->language])) {
            $this->readDatafile($liveTranslatorString->language, ($namespace ?: $this->namespace));
        }
        $this->internalStorage[($namespace ?: $this->namespace)][$liveTranslatorString->language][$liveTranslatorString->id] = $liveTranslatorString;
        $this->internalStorageUpdated[($namespace ?: $this->namespace)][$liveTranslatorString->language] = true;
        return $this;
    }

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return IStorage
     */
    public function removeLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): IStorage {
        if (!isset($this->internalStorage[($namespace ?: $this->namespace)][$liveTranslatorString->language])) {
            $this->readDatafile($liveTranslatorString->language, ($namespace ?: $this->namespace));
        }
        unset($this->internalStorage[($namespace ?: $this->namespace)][$liveTranslatorString->language][$liveTranslatorString->id]);
        $this->internalStorageUpdated[($namespace ?: $this->namespace)][$liveTranslatorString->language] = true;
        return $this;
    }

    /**
     * 
     */
    public function __destruct() {
        foreach ($this->internalStorageUpdated as $namespace => $languages) {
            foreach ($languages as $language => $true) {
                if (!$true) {
                    continue;
                }
                $this->writeDatafile($language, $namespace);
            }
        }
    }

    /**
     * 
     * @param string $language
     * @param string $namespace
     * @return \SplFileInfo
     */
    protected function getFilename($language, $namespace = NULL) {
        return new \SplFileInfo($this->storageDir . DIRECTORY_SEPARATOR . $language . '-' . ($namespace ?: $this->namespace) . '.slng');
    }

    /**
     * 
     * @param string $language
     * @param string $namespace
     * @return $this
     * @throws Exception
     */
    protected function writeDatafile($language, $namespace = NULL) {
        $file = $this->getFilename($language, $namespace);
        try {
            $fileobj = $file->openFile('w');
            $contents = serialize($this->internalStorage[$namespace][$language]);
            $fileobj->fwrite($contents, strlen($contents));
            return $this;
        } catch (\Exception $e) {
            throw new \Exception('Could not write file : ' . $file);
        }
    }

    /**
     * 
     * @param string $language
     * @param string $namespace
     * @return $this
     * @throws Exception
     */
    protected function readDatafile($language, $namespace = NULL) {
        $file = $this->getFilename($language, $namespace);
        if (!$file->isFile()) {
            return $this;
        }
        if ($file->isReadable()) {
            $fileobj = $file->openFile('r');
            $contents = $fileobj->fread($fileobj->getSize());
            $this->internalStorage[$namespace][$language] = unserialize($contents);
            return $this;
        }
        throw new \Exception('Could not read file : ' . $file);
    }

}
