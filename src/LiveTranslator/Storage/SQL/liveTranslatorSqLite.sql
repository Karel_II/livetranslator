CREATE TABLE `liveTranslatorOriginals` (
	`id`	TEXT NOT NULL PRIMARY KEY,
	`namespace`	TEXT ,
	`original`	TEXT NOT NULL ,
	`timeStamp`	TIMESTAMP, 
        UNIQUE(`namespace`, `original`) ON CONFLICT REPLACE
);

CREATE TABLE `liveTranslatorTranslations` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`originalsId`	INTEGER NOT NULL  REFERENCES liveTranslatorOriginals(id) ON UPDATE CASCADE,
	`language`	TEXT NOT NULL ,
	`nounNumber`	INTEGER NOT NULL DEFAULT 0,
	`translation`	TEXT NOT NULL,
	`timeStamp`	TIMESTAMP, 
        UNIQUE(`originalsId`, `language`,`nounNumber`) ON CONFLICT REPLACE
);

CREATE TRIGGER `liveTranslatorOriginalsTrigger` AFTER UPDATE ON `liveTranslatorOriginals`
BEGIN
      update `liveTranslatorOriginals` SET `timeStamp` = datetime('now') WHERE `id` = NEW.`id`;
END;
CREATE TRIGGER `liveTranslatorTranslationsTrigger` AFTER UPDATE ON `liveTranslatorTranslations`
BEGIN
      update `liveTranslatorTranslations` SET `timeStamp` = datetime('now') WHERE `id` = NEW.`id`;
END;