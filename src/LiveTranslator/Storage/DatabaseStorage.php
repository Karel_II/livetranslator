<?php

namespace LiveTranslator\Storage;

use Nette,
    Nette\Database\Context;
use LiveTranslator\IStorage,
    LiveTranslator\LiveTranslatorString,
    LiveTranslator\Language;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DatabaseStorage implements IStorage {

    use Nette\SmartObject;

    const
            TABLE_NAME_ORIGINALS = 'liveTranslatorOriginals',
            TABLE_NAME_TRANSLATIONS = 'liveTranslatorTranslations';

    /** @var Nette\Database\Connection */
    protected $connection;

    /** @var Nette\Database\Context */
    protected $context;

    public function __construct(Context $context) {
        $this->context = $context;
        $this->connection = $context->getConnection();
    }

    public function countLiveTranslatorStrings(Language $language, $namespace = NULL): int {
        return $this->context->table(self::TABLE_NAME_ORIGINALS)->where('namespace', $namespace)->where('language', $language->name)->count();
    }

    public function fetchLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): bool {
        $resourceTranslations = $this->context->table(self::TABLE_NAME_TRANSLATIONS)->select('*')->where('namespace', $namespace)->where('language', $liveTranslatorString->language)->where('originalId', $liveTranslatorString->id);
        if ($resourceTranslations) {
            foreach ($resourceTranslations as $row) {
                $liveTranslatorString->translations[$row['nounNumber']] = $row['translation'];
            }
            return true;
        }
        return false;
    }

    public function getAllLiveTranslatorStrings(Language $language, $namespace = NULL): array {
        $return = array();
        $resourceOriginals = $this->context->table(self::TABLE_NAME_ORIGINALS)->select('*')->where('namespace', $namespace)->where('language', $language->name);
        foreach ($resourceOriginals as $row) {
            $liveTranslatorString = LiveTranslatorString::newString($row['original'], NULL, $row['language']);
            $return[$liveTranslatorString->id] = $liveTranslatorString;
        }
        $resourceTranslations = $this->context->table(self::TABLE_NAME_TRANSLATIONS)->select('*')->where('namespace', $namespace)->where('language', $language->name);
        foreach ($resourceTranslations as $row) {
            $return[$row->id]->translations[$row['nounNumber']] = $row['translation'];
        }
        return $return;
    }

    public function getLiveTranslatorString($id, \LiveTranslator\Language $language, $namespace = NULL): LiveTranslatorString {
        $liveTranslatorString = NULL;
        $resourceOriginals = $this->context->table(self::TABLE_NAME_ORIGINALS)->select('*')->where('namespace', $namespace)->where('language', $language->name)->where('id', $id);
        if (($row = $resourceOriginals->fetch())) {
            $liveTranslatorString = LiveTranslatorString::newString($row['original'], NULL, $row['language']);
        }
        $resourceTranslations = $this->context->table(self::TABLE_NAME_TRANSLATIONS)->select('*')->where('namespace', $namespace)->where('language', $language->name)->where('originalId', $id);
        foreach ($resourceTranslations as $row) {
            $liveTranslatorString->translations[$row['nounNumber']] = $row['translation'];
        }
        return $liveTranslatorString;
    }

    public function removeLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): IStorage {
        $this->context->table(self::TABLE_NAME_TRANSLATIONS)->delete()->where('namespace', $namespace)->where('originalsId', $liveTranslatorString->id);
        $this->context->table(self::TABLE_NAME_ORIGINALS)->delete()->where('namespace', $namespace)->where('id', $liveTranslatorString->id);
        return $this;
    }

    public function setLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): IStorage {
        $insertOriginal = array();
        $insertOriginal['id'] = $liveTranslatorString->id;
        $insertOriginal['namespace'] = $namespace ?: NULL;
        $insertOriginal['language'] = $liveTranslatorString->language;
        $insertOriginal['original'] = $liveTranslatorString->original;
        $this->connection->query('INSERT OR IGNORE INTO ' . self::TABLE_NAME_ORIGINALS . '', $insertOriginal);
        $insertTranslations = array();
        if (empty($liveTranslatorString->translations)) {
            return $this;
        }
        foreach ($liveTranslatorString->translations as $key => $translation) {
            $insertTranslation = array();
            $insertTranslation['originalsId'] = $liveTranslatorString->id;
            $insertTranslation['namespace'] = $namespace ?: NULL;
            $insertTranslation['language'] = $liveTranslatorString->language;
            $insertTranslation['nounNumber'] = $key;
            $insertTranslation['translation'] = $translation;
            $insertTranslations[] = $insertTranslations;
        }
        return $this;
    }

}
