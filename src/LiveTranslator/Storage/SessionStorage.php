<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator\Storage;

use LiveTranslator\IStorage,
    LiveTranslator\LiveTranslatorString,
    LiveTranslator\Language;
use Nette\Http\Session;

/**
 * Description of Session
 *
 * @author karel.novak
 */
class SessionStorage implements IStorage {

    private $session = NULL;
    private $namespace = 'default';

    protected function getSessionSection($namespace) {
        $ns = $namespace ?: $this->namespace;
        return $this->session->getSection("LiveTranslatorStorageSession-$ns");
    }

    public function __construct(Session $session) {
        $this->session = $session;
        $this->session->start();
    }

    /**
     * 
     * @param Language $language
     * @param string $namespace
     * @return array
     */
    public function getAllLiveTranslatorStrings(Language $language, $namespace = NULL): array {
        $return = array();
        $section = $this->getSessionSection($namespace);
        if (!isset($section->strings)) {
            $section->strings = array();
        }
        if (isset($section->strings) && isset($section->strings[$language->name]) && $section->strings[$language->name] !== NULL) {
            foreach ($section->strings[$language->name] as $liveTranslatorStringEncoded) {
                $liveTranslatorString = unserialize($liveTranslatorStringEncoded);
                $return[$liveTranslatorString->id] = $liveTranslatorString;
            }
            return $return;
        }
        return array();
    }

    /**
     * 
     * @param string $id
     * @param Language $language
     * @param string $namespace
     * @return LiveTranslatorString
     */
    public function getLiveTranslatorString($id, Language $language, $namespace = NULL): LiveTranslatorString {
        $section = $this->getSessionSection($namespace);
        if (!isset($section->strings)) {
            $section->strings = array();
        }
        return unserialize($section->strings[$language->name][$id]);
    }

    /**
     * 
     * @param Language $language
     * @param string $namespace
     * @return int Description
     */
    public function countLiveTranslatorStrings(Language $language, $namespace = NULL): int {
        $section = $this->getSessionSection($namespace);
        if (!isset($section->strings)) {
            $section->strings = array();
        }
        if (isset($section->strings) && isset($section->strings[$language->name]) && $section->strings[$language->name] !== NULL) {
            return count($section->strings[$language->name]);
        }
        return 0;
    }

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return bool
     */
    public function fetchLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): bool {
        $section = $this->getSessionSection($namespace);
        if (!isset($section->strings)) {
            $section->strings = array();
        }
        if (isset($section->strings[$liveTranslatorString->language][$liveTranslatorString->id])) {
            $liveTranslatorStringFetch = unserialize($section->strings[$liveTranslatorString->language][$liveTranslatorString->id]);
            $liveTranslatorString->translations = $liveTranslatorStringFetch->translations;
            $liveTranslatorString->deleted = $liveTranslatorStringFetch->deleted;
            return true;
        }
        $liveTranslatorString->translations = false;
        return false;
    }

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return IStorage
     */
    public function removeLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): IStorage {
        $section = $this->getSessionSection($namespace);
        if (!isset($section->strings)) {
            $section->strings = array();
        }
        unset($section->strings[$liveTranslatorString->language][$liveTranslatorString->id]);
        return $this;
    }

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return IStorage
     */
    public function setLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): IStorage {
        $section = $this->getSessionSection($namespace);
        if (!isset($section->strings)) {
            $section->strings = array();
        }
        $section->strings[$liveTranslatorString->language][$liveTranslatorString->id] = serialize($liveTranslatorString);
        return $this;
    }

}
