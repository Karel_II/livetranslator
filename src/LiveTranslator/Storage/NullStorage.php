<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator\Storage;

use LiveTranslator\IStorage,
    LiveTranslator\LiveTranslatorString,
    LiveTranslator\Language;

/**
 * Description of NullStorage
 *
 * @author karel.novak
 */
class NullStorage implements IStorage {

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param type $namespace
     * @return bool
     */
    public function fetchLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): bool {
        return true;
    }

    /**
     * 
     * @param Language $language
     * @param string $namespace
     * @return array
     */
    public function getAllLiveTranslatorStrings(Language $language, $namespace = NULL): array {
        return array();
    }

    /**
     * 
     * @param string $id
     * @param Language $language
     * @param string $namespace
     * @return LiveTranslatorString
     */
    public function getLiveTranslatorString($id, Language $language, $namespace = NULL): LiveTranslatorString {
        return NULL;
    }

    /**
     * 
     * @param Language $language
     * @param string $namespace
     * @return int Description
     */
    public function countLiveTranslatorStrings(Language $language, $namespace = NULL): int {
        return 0;
    }

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return IStorage
     */
    public function removeLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): IStorage {
        return $this;
    }

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return IStorage
     */
    public function setLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL): IStorage {
        return $this;
    }

}
