<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator;

use LiveTranslator\LiveTranslatorString;

/**
 *
 * @author karel.novak
 */
interface IStorage {

    /**
     * 
     * @param string $id
     * @param Language $language
     * @param string $namespace
     * @return \LiveTranslator\LiveTranslatorString 
     */
    function getLiveTranslatorString($id, Language $language, $namespace = NULL);

    /**
     * 
     * @param Language $language
     * @param string $namespace
     * @return array Description
     */
    function getAllLiveTranslatorStrings(Language $language, $namespace = NULL);

    /**
     * 
     * @param Language $language
     * @param string $namespace
     * @return int Description
     */
    function countLiveTranslatorStrings(Language $language, $namespace = NULL);

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return bool
     */
    function fetchLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL);

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param string $namespace
     * @return \LiveTranslator\IStorage
     */
    function setLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL);

    /**
     * 
     * @param LiveTranslatorString $liveTranslatorString
     * @param type $namespace
     * @return \LiveTranslator\IStorage
     */
    function removeLiveTranslatorString(LiveTranslatorString $liveTranslatorString, $namespace = NULL);
}
