<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator;

/**
 * Description of String
 *
 * @author Karel
 * 
 * @property type $original Description
 * @property type $translations Description
 * @property type $language Description
 * @property bool $deleted Description
 * @property-read string $id Description
 * @property-read bool $translated Description
 */
class LiveTranslatorString {

    use \Nette\SmartObject;

    private $original = null;
    private $translations = FALSE;
    private $language = NULL;
    private $deleted = FALSE;

    public function getOriginal() {
        return $this->original;
    }

    public function getTranslations() {
        return $this->translations;
    }

    public function getLanguage() {
        return $this->language;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function getId() {
        return md5($this->original . '#' . $this->language);
    }

    public function setOriginal($original) {
        $this->original = trim($original);
        return $this;
    }

    public function setTranslations($translations) {
        if (is_array($translations)) {
            $this->translations = $translations;
        } elseif ($translations === false || preg_match('/false/i', $translations)) {
            $this->translations = false;
        }
        return $this;
    }

    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    public function setDeleted($deleted) {
        if ($deleted === true || preg_match('/true/i', $deleted)) {
            $this->deleted = true;
        } else {
            $this->deleted = false;
        }
        return $this;
    }

    public function getTranslation($nounNumber) {
        return (!$this->deleted && $this->translations !== FALSE && is_array($this->translations) && isset($this->translations[$nounNumber]) && is_string($this->translations[$nounNumber])) ? $this->translations[$nounNumber] : FALSE;
    }

    public function isTranslated() {
        return !$this->deleted && $this->translations !== FALSE && is_array($this->translations);
    }

    public function toArray() {
        return array(
            'original' => $this->original,
            'translations' => $this->translations,
            'language' => $this->language,
            'deleted' => $this->deleted,
            'translated' => $this->isTranslated(),
        );
    }

    public static function newString($original, $translations, $language, $deleted = false) {
        $return = new static;
        $return->setLanguage($language);
        $return->setOriginal($original);
        $return->setTranslations($translations);
        $return->setDeleted($deleted);
        return $return;
    }

    public static function fromRequest($request) {
        $return = new static;
        if (isset($request) && is_array($request)) {
            $return = new static;
            $return->setLanguage($request['language']);
            $return->setOriginal($request['original']);
            $return->setTranslations($request['translations']);
            $return->setDeleted($request['deleted']);
            return $return;
        }
        return NULL;
    }

}
