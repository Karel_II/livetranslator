<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator\Latte\Runtime;

/**
 * Description of FilterOrdinal
 *
 * @author Karel
 */
class FilterOrdinal {

    /**
     * 
     * @param int $order
     * @return string
     */
    public static function getOrdinalString($order): string {
        switch (substr($order, -1)) {
            case 1:
                return 'st';
            case 2:
                return 'nd';
            case 3:
                return 'rd';
            default:
                return 'th';
        }
    }

    /**
     * 
     * @param int $order
     * @return string
     */
    public function __invoke($order): string {
        return self::getOrdinalString($order);
    }

}
