<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LiveTranslator\DI;

use \LiveTranslator\Diagnostics\Panel,
    \LiveTranslator\Storage\NullStorage,
    \LiveTranslator\Translator,
    \LiveTranslator\Application\UI\ITracyControlFactory;

/**
 * Description of LiveTranslatorExtension
 *
 * @author karel.novak
 */
class LiveTranslatorExtension extends \Nette\DI\CompilerExtension {

    private $defaults = [
        'defaultLanguage' => 'sy-SYSTEM',
        'availableLanguages' => ['sy-SYSTEM'],
        'diagnosticsLayout' => Panel\LiveTranslator::LAYOUT_VERTICAL,
    ];

    /**
     * 
     * @return void
     */
    public function loadConfiguration() {
        $this->validateConfig($this->defaults);

        $builder = $this->getContainerBuilder();

        try {
            $builder->getDefinition($this->prefix('translatedStorage'));
        } catch (\Exception $e) {
            $builder->addDefinition($this->prefix('translatedStorage'))
                    ->setFactory(NullStorage::class);
        }
        try {
            $builder->getDefinition($this->prefix('untranslatedStorage'));
        } catch (\Exception $e) {
            $builder->addDefinition($this->prefix('untranslatedStorage'))
                    ->setFactory(NullStorage::class);
        }
        $builder->addDefinition($this->prefix('translator'))
                ->setFactory(Translator::class, [$this->prefix('@translatedStorage'), $this->prefix('@untranslatedStorage')])
                ->addSetup('setupAvailableLanguage', [$this->config['availableLanguages']])
                ->addSetup('setDefaultLanguage', [$this->config['defaultLanguage']]);

        $builder->addDefinition($this->prefix('diagnostics'))
                ->setFactory(Panel\LiveTranslator::class)
                ->addSetup('setLayout', [$this->config['diagnosticsLayout']]);

        $builder->getDefinition('tracy.bar')
                ->addSetup('addPanel', [$this->prefix('@diagnostics')]);

        $builder->addDefinition($this->prefix('tracyControlFactory'))
                ->setImplement(ITracyControlFactory::class);

        $builder->addAlias('translator', $this->prefix('translator'));
    }

}
