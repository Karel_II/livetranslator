<?php

spl_autoload_register(function ($className) {
    static $classMap = [
        /**
         * LiveTranslator\\Application\\UI
         */
        'LiveTranslator\\Application\\UI\\ITracyControlFactory' => 'Application/UI/LiveTranslatorExtension.php',
        'LiveTranslator\\Application\\UI\\TracyControl' => 'Application/UI/TracyControl.php',
        /**
         * LiveTranslator\\DI
         */
        'LiveTranslator\\DI\\LiveTranslatorExtension' => 'DI/LiveTranslatorExtension.php',
        /**
         * LiveTranslator\\Diagnostics\\Panel
         */
        'LiveTranslator\\Diagnostics\\Panel\\LiveTranslator' => 'Diagnostics/Panel/LiveTranslator.php',
        /**
         * LiveTranslator\\Latte\\Runtime
         */
        'LiveTranslator\\Latte\\Runtime\\FilterOrdinal' => 'Latte/Runtime/FilterOrdinal.php',
        /**
         * LiveTranslator\\Storage
         */
        'LiveTranslator\\Storage\\FileStorage' => 'Storage/FileStorage.php',
        'LiveTranslator\\Storage\\NetteDatabase' => 'Storage/Database.phpStorage',
        'LiveTranslator\\Storage\\NullStorage' => 'Storage/NullStorage.php',
        'LiveTranslator\\Storage\\SessionStorage' => 'Storage/SessionStorage.php',
        /**
         * LiveTranslator
         */
        'LiveTranslator\\IStorage' => 'Storage/IStorage.php',
        'LiveTranslator\\Language' => 'Storage/Language.php',
#        'LiveTranslator\\LiveTranslatorString' => 'Storage/LiveTranslatorString.php',
        /**
         * Translator
         */
        'LiveTranslator\\Translator' => 'Translator.php',
    ];
    if (isset($classMap[$className])) {
        require __DIR__ . '/LiveTranslator/' . $classMap[$className];
    }
});

